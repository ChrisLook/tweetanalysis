sed -i -- 's/Intel/INTC/g' $1
sed -i -- 's/JPMorganChase/JPM/g' $1
sed -i -- 's/Google/GOOG/g' $1
sed -i -- 's/Boeing/BA/g' $1
sed -i -- 's/Merck/MRK/g' $1
sed -i -- 's/Apple/AAPL/g' $1
sed -i -- 's/Procter&Gamble/PG/g' $1
sed -i -- 's/Walmart/WMT/g' $1
